package com.example.appone.restapi;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloRestController {

    @GetMapping("hello")
    public String hello(){
        return "HelloRestController";
    }


    @GetMapping("super")
    public String mySuper() {
        return "My Supper";
    }

}
